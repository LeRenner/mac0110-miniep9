using LinearAlgebra

function multiplica(a, b)
  dima = size(a)
  dimb = size(b)
  if dima[2] != dimb[1] 
    return -1 
  end
  c = zeros(dima[1], dimb[2])
  for i in 1:dima[1]
    for j in 1:dimb[2]
      for k in 1:dima[2]
        c[i, j] = c[i, j] + a[i, k] * b[k, j]
      end
    end
  end
  return c
end

function matrix_pot(M, p)
  index = 1
  temp_matrix = M
  while index < p
    temp_matrix = multiplica(temp_matrix, M)
    index += 1
  end
  return temp_matrix
end

function matrix_pot_by_squaring(M, p)
  if p == 0
    return LinearAlgebra.I
  elseif p == 1
    return M
  elseif p < 0
    println("Well... That was certainly unexpected.")
  elseif p % 2 == 0
    return matrix_pot_by_squaring(multiplica(M, M), p/2)
  else
    return multiplica(M, matrix_pot_by_squaring(multiplica(M, M), (p-1)/2))
  end
end

function testes()

  println("[1 2; 3 4] ^ 18 usando matrix_pot()")
  @time matrix_pot([1 2; 3 4], 18)
  println("[1 2; 3 4] ^ 18 usando matrix_pot_by_squaring()")
  @time matrix_pot_by_squaring([1 2; 3 4], 18)
  
  println("[1 2 -7; 3 4 18; 5 11 11] ^ 12 using matrix_pot()")
  @time matrix_pot([1 2 -7; 3 4 18; 5 11 11], 12)
  println("[1 2 -7; 3 4 18; 5 11 11] ^ 12 using matrix_pot_by_squaring()")
  @time matrix_pot_by_squaring([1 2 -7; 3 4 18; 5 11 11], 12)

end

testes()
